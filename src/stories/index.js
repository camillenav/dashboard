import React from 'react';

import { storiesOf } from '@storybook/react';

//import { Button, Welcome } from '@storybook/react/demo';
import Identite from '../Components/Identite/Identite';
import Heure from '../Components/Heure/Heure';
import Graph from '../Components/Graph/Graph';
import Mot from '../Components/Mot/Mot';
import Argent from '../Components/Argent/Argent';
import Souris from '../Components/Souris/Souris';

storiesOf('Bonjour', module)
    .add('Camille', () => <Identite photo="https://pbs.twimg.com/profile_images/1084591284378091530/NKF3PwF9_400x400.jpg"
                                    name="Camille" />)
    .add('Roman', () => <Identite photo="https://media.licdn.com/dms/image/C4D03AQHAwbI_EEIPZA/profile-displayphoto-shrink_200_200/0?e=1556755200&v=beta&t=VYBE3_YwAJTGc6cO3_vVt1HbHz27xV45_f2SWK6vm6k"
                                    name="Roman" />);

storiesOf('Heure', module)
    .add('Actuelle', () => <Heure />);

storiesOf('Graph', module)
    .add('Motivation', () => <Graph data={[
        {
            name: 'Lundi', motivation: 1,
        },
        {
            name: 'Mardi', motivation: 10,
        },
        {
            name: 'Mercredi', motivation: 17,
        },
        {
            name: 'Jeudi', motivation: 15,
        },
        {
            name: 'Vendredi', motivation: 40,
        },
        {
            name: 'Samedi', motivation: 29,
        },
        {
            name: 'Dimanche', motivation: 0,
        }
    ]}
        width={550}
        height={300}
        legende="motivation" />);

storiesOf('Mot', module)
    .add('du jour', () => <Mot liste={["fraise",
        "combat",
        "aphrodisiaque",
        "louveteau",
        "escargot",
        "sapristi",
        "polichinelle",
        "grapiller",
        "cirque",
        "palindrome",
        "futile",
        "multiplication",
        "quartz",
        "carapace",
        "draconienne",
        "embellir",
        "jungle",
        "topinambour",
        "balivernes",
        "saltimbanque",
        "sparadrap"
    ]} />);

storiesOf('Compte', module)
    .add('Banquaire', () => <Argent argent={(Math.random() * 500).toFixed(2)} compte="compte banquaire" />)
    .add('Livret A', () => <Argent argent={(Math.random() * 2000).toFixed(2)} compte="livret A"/>);

storiesOf('Souris', module)
    .add('Coordonnées', () => <Souris />);

