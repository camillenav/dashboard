import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Form from './Components/Form/Form';
import Accueil from './Components/Accueil/Accueil';

export default () => (

    <Switch>
        <Route path="/" component={Accueil} exact />
        <Route path="/admin" component={Form} />
    </Switch>
);