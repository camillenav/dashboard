import React, { Component } from 'react';
import './Identite.css';
import axios from 'axios';

class Identite extends Component {

constructor(props) {
		super(props)

		this.state = {
			album: null
		}
	}

	componentDidMount() {
		this.sync()
	}

    render() {
		const { album } = this.state;
        
		console.log(album)

		if (album == null) {
			return <p>L'album du moment est introuvable </p>
		}

		return (
			<p>L'album du moment est : { album.Title }
			<img className="photo" src={ album.Cover_url}/></p>
		);
       
    }
	
	sync() {
		axios.get("http://localhost:8000/album/alea")
			.then((res) => this.setState({ album: res.data[0] }))
	}

}
export default Identite;
