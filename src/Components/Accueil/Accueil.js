import React, { Component } from 'react';
import Identite from '../Identite/Identite';
import Heure from '../Heure/Heure';
import Graph from '../Graph/Graph';
import Mot from '../Mot/Mot';
import Argent from '../Argent/Argent';
import Souris from '../Souris/Souris';
import Pie from '../Pie/Pie';
import './Accueil.css';


const data = [{
    name: 'Lundi', motivation: 1,
},
{
    name: 'Mardi', motivation: 10,
},
{
    name: 'Mercredi', motivation: 17,
},
{
    name: 'Jeudi', motivation: 15,
},
{
    name: 'Vendredi', motivation: 40,
},
{
    name: 'Samedi', motivation: 29,
},
{
    name: 'Dimanche', motivation: 0,
    }];


class Accueil extends Component {


    render() {
        return (
		
            <div className="container">

			


                <div className="row">
                    <div className="widget col-6 col-md-4 col-lg-3">
                        <Identite
                            photo="https://pbs.twimg.com/profile_images/1084591284378091530/NKF3PwF9_400x400.jpg"
                            name="Camille" />
                    </div>
                    <div className="widget col-6 col-md-4 col-lg-3">
                        <Heure />
                    </div>
                    <div className="col-6 col-md-4 col-lg-3">
                        <div className="widget col-12">
                            <Mot/>
                        </div>
                        <div className="widget col-12">
                            <Argent
                                argent={(Math.random() * 500).toFixed(2)}
                                compte="compte" />
                        </div>
                    </div>
                    <div className="widget col-6 col-md-4 col-lg-3">
                        <Souris />
                    </div>
                    <div className="widget col-6">
                        <Graph
                            data={data}
                            height={300}
                            legende="motivation" />
                    </div>
                    <div class="widget col-6">
                        <Pie/>
                    </div>
                </div>
            </div>
        );
    }

}
export default Accueil;