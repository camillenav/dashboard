import React, { Component } from 'react';
import './Souris.css';
import axios from 'axios';

class Souris extends Component {

    constructor(props) {
        super(props);

        this.state = { 
			artists: [],
			albums: [],
			titres: []
		};
    }

	componentWillMount() {
		this.sync();
	}
   
    render() {
        const { artists, albums, titres } = this.state;

        return (
            <p><h3>Chiffres clefs</h3><br/>
				Nombre d'artistes : <span className="coord">{this.state.artists.length}</span><br/>
				
				Nombre d'albums : <span className="coord">{this.state.albums.length}</span><br/>
				
				Nombre de morceaux : <span className="coord">{this.state.titres.length}</span><br/>
			</p>
        
        );
    }

	sync() {
		axios.get("http://localhost:8000/artists")
				.then((res) => this.setState({artists: res.data}))

		axios.get("http://localhost:8000/album")
				.then((res) => this.setState({albums: res.data}))

		axios.get("http://localhost:8000/track")
				.then((res) => this.setState({titres: res.data}))
	}

}
export default Souris;