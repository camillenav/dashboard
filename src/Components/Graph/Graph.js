import React, { Component } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

class Graph extends Component {
    
    render() {
        const { data, height, legende } = this.props;

        return (
            <ResponsiveContainer width="100%"
                height={height}>
                <LineChart
                    data={data}
                    margin={{
                        top: 15, right: 30, left: 0, bottom: 15,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Line type="monotone" dataKey={legende} stroke="#008B8B" />
                </LineChart>
            </ResponsiveContainer>
        );
    }
}
export default Graph;