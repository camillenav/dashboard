import React, { Component } from 'react';
import './Argent.css';
import axios from 'axios';
import moment from 'moment';
import 'moment/locale/fr';

class Argent extends Component {
	 constructor(props) {
			super(props);

			this.state = { 
				track: []
			};
		}

	componentWillMount() {
		this.sync();
	}

    render() {
        const { track } = this.state;
		let temps = 0;
        
		for(let i = 0; i < this.state.track.length; i++) {
			temps += this.state.track[i].Duration*this.state.track[i].Listenings;
		}

		temps = moment.duration(temps, 'minute');

        return (
            <p>Vous avez <span className="argent">{temps.locale("fr").humanize()}</span> de musique !</p>
        );
    }

	sync() {
		axios.get("http://localhost:8000/track")
				.then((res) => this.setState({track: res.data}))

	}

}
export default Argent;