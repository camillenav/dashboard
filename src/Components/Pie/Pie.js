import React, { PureComponent } from 'react';
import {
    PieChart, Pie, Sector, Cell, Legend
} from 'recharts';
import axios from 'axios';


const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
    cx, cy, midAngle, innerRadius, outerRadius, percent, index,
}) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
        <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
            {`${(percent * 100).toFixed(0)}%`}
        </text>
    );
};

class Camembert extends PureComponent {
    static jsfiddleUrl = 'https://jsfiddle.net/alidingling/k9jkog04/';
	
	constructor(props) {
		super(props)

		this.state = {
			genre: null,
			num : null,
			 data: []
		}
	}

	componentDidMount() {
		this.sync()
	}

    render() {
		
		const{genre} = this.state;
		const{num}=this.state;
		const { data } = this.state;

		

        return (
            <PieChart width={400} height={400}>
                <Pie
                    data ={data}
                    cx={200}
                    cy={200}
                    labelLine={false}
                    label={renderCustomizedLabel}
                    outerRadius={80}
                    fill="#8884d8"
                    dataKey="nbm"
					nameKey="_id"
                >
                    {
                        data.map((genre, num) => <Cell key={`cell-${num}`} fill={COLORS[num % COLORS.length]} />)
                    }
                    
                </Pie>
                <Legend />
            </PieChart>
        );
    }

	sync() {
		axios.get("http://localhost:8000/album/genre")
			.then((rep) => this.setState({ data : rep.data }))
	}
}
export default Camembert;
