import React, { Component } from 'react';
import './Form.css';
import axios from 'axios';

class Form extends Component {
    constructor(props) {
        super(props)

        this.state = {
            artists: [],
            name: "",
            birth: "",
            flwers: ""

        }

		this.onSubmit = this.onSubmit.bind(this);
    }

	componentWillMount() {
		this.getArtists();
	}

    render() {
		const artists = [];
		for(let i = 0; i < this.state.artists.length; i++) {
			artists.push(<p>{this.state.artists[i].Nom} - {this.state.artists[i].Birth} - {this.state.artists[i].Followers}</p>);
		}
        return (
           <div> <form>
                <h3>Formulaire</h3>
                <label>Artiste :
                    <input class="champ" type="text" value={this.state.name} onChange={e => this.setState({'name': e.target.value})} />
                </label><br />
                <label>Date de naissance :
                    <input class="champ" type="date" value={this.state.birth} onChange={e => this.setState({'birth': e.target.value})} />
                </label><br />
	            <label>
                    Followers :
                    <input class="champ" type="number" value={this.state.flwers} onChange={e => this.setState({'flwers': e.target.value})} />
                </label><br />
                <input class="valide" type="submit" value="Submit" onClick={this.onSubmit} />
            </form>

			<div>
				{artists}
			</div>
			</div>
        );
    }

    onSubmit(e) {
		e.preventDefault();
		e.stopPropagation();
        if (this.state.name.length === 0 || this.state.birth.length === 0 || this.state.birth.length === 0) {
            alert("form incomplet !");
        }
        else this.sendForm();
    }

	getArtists() {
		axios.get("http://localhost:8000/artists")
				.then((res) => this.setState({artists: res.data}))
	}
	
    sendForm() {
		

        axios.put("http://localhost:8000/artists", {
            Nom: this.state.name,
            Birth: this.state.birth,
            Followers: this.state.flwers
        })
    }

}
export default Form;