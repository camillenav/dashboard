import React, { Component } from 'react';
import './Heure.css';

import moment from 'moment';
import 'moment/locale/fr';

class Heure extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: moment()
        };
    }

    componentDidMount() {
        setInterval(() => {
            this.setState({
                date: moment()
            });
        }, 1000)
    } 


    render() {
        return (
            <p>
                Nous sommes le <br /><span className="temps">{this.state.date.format("DD / MM / YYYY")}</span><br/>
                Il est <br /> <span className="temps">{this.state.date.format("HH : mm")}</span>
            </p>
        );
    } 
}
export default Heure;