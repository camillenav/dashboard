import React, { Component } from 'react';
import './App.css';
import Routes from './Routes';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';



class App extends Component {
	
    render() {

	
        return (
		
            <div className="container">
              
				
                <Routes />

           </div>     
          
        );
    }
}

export default App;
